import { Component, Inject,  OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { User } from '../../model/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

	public users:User[] | null = null;

  constructor(@Inject(UsersService) private userService:UsersService){

  	userService.getAll().subscribe(r=>this.users = r.users)
  }

}
