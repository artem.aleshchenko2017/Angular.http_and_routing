import {Component, Input, Inject, Injectable, OnInit} from '@angular/core';
import {Note} from '../../model/note';
import {NotesService} from '../../services/notes.service';

@Component({
  selector: 'app-active-note',
  template: `
  	<div class="note" *ngIf="note!==null">
        <div class="name">{{note.name}}</div>
        <div class="text">{{note.text}}</div>
    </div>`
    ,
  styleUrls: ['./active-note.component.css']
})
export class ActiveNoteComponent {

	public note:Note|null = null;

  constructor(@Inject(NotesService) private noteService:NotesService){
    noteService.getActiveNote().subscribe(n=>this.note = n);

  }
}
