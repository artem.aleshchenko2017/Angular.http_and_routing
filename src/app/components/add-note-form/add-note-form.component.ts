import { Component, OnInit } from '@angular/core';
import {Note} from '../../model/note';
import {NotesService} from '../../services/notes.service';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-add-note-form',
  template:
  `
   <div class="form">
      <dl>
        <dt><label for="note-name-input">name</label></dt>
        <dd><input type="text" id="note-name-input" [(ngModel)]="note.name" placeholder="name"></dd>

        <dt><label for="note-text-input">text</label></dt>
        <dd><textarea id="note-text-input" [(ngModel)]="note.text" placeholder="text"></textarea></dd>
        <dd><button (click)="addNote()" class="btn add">add</button></dd>
      </dl>
    </div>
  `,
  styleUrls: ['./add-note-form.component.css']
})

export class AddNoteFormComponent{
  public note:Note = new Note('', '');
   constructor(@Inject(NotesService) private noteService:NotesService){

  }


  addNote(){
  	this.noteService.addNote(this.note);
  	this.note = new Note('','');
  }
}
