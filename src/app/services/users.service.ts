import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserResponse } from '../model/users-response';
import { map } from 'rxjs/operators';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  

  static PREFIX = 'https://mag-contacts-api.herokuapp.com'
  constructor(@Inject(HttpClient)private http:HttpClient) { }

  getAll():Observable<UserResponse>{
  	return this.http.get(UsersService.PREFIX + '/users')
  	  .pipe(map(r=>UserResponse.fromJson(r)))
  }
}
