import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app.component';
// ../components/app.component
import { FormsModule } from '@angular/forms';
import { ActiveNoteComponent } from './components/active-note/active-note.component';
import { AddNoteFormComponent } from './components/add-note-form/add-note-form.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { NotesComponent } from './components/notes/notes.component';
import { UsersComponent } from './components/users/users.component';
import { RouterModule, Routes } from '@angular/router';

let routes:Routes = [
  {path:"notes", component:NotesComponent},
  {path:"users", component:UsersComponent},
  {path:"", redirectTo:"notes", pathMatch:"full"}
];

@NgModule({
  declarations: [
    AppComponent,
    ActiveNoteComponent,
    AddNoteFormComponent,
    SidebarComponent,
    NotesComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
