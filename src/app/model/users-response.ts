
import {User} from "./user";

export class UserResponse{
	constructor(
		public status:string,
		public users:User[]|null,
		public error: string|null
		){

	}


	static fromJson(r:any){

		let users: User[]|null = null;
		if(r.users){
			users = (r.users as any[]).map(u=>User.fromJson(u))

		}


		return new UserResponse(
			r.status,
			users,
			r.error ? r.error:null
			)
	}
}